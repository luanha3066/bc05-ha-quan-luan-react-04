import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART, CHANGE_DETAIL } from "./redux/ShoeConstant";

class ItemShoe extends Component {
  render() {
    let { image, name } = this.props.data;
    return (
      <div className="col-3 p-1 h-">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title h-50">{name}</h4>
            <button
              onClick={() => this.props.handleBuyShoe(this.props.data)}
              className="btn btn-success mr-1"
            >
              Mua
            </button>
            <button
              onClick={() => this.props.handleChangeShoe(this.props.data)}
              className="btn btn-primary"
            >
              Xem chi tiet
            </button>
          </div>
        </div>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handleBuyShoe: (shoe) => {
      let action = {
        type: ADD_TO_CART,
        payload: shoe,
      };
      dispatch(action);
    },
    handleChangeShoe: (shoe) => {
      let action = {
        type: CHANGE_DETAIL,
        payload: shoe,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(ItemShoe);
