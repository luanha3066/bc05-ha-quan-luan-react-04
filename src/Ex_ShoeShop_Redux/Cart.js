import React, { Component } from "react";
import { connect } from "react-redux";
import { MINUS_SHOE, PLUS_SHOE } from "./redux/ShoeConstant";

class Cart extends Component {
  renderTbody = () => {
    return this.props.gioHang.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.number}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleMinusShoe(item);
              }}
              className="btn btn-primary mr-1"
            >
              -
            </button>
            {item.number}
            <button
              onClick={() => {
                this.props.handlePlusShoe(item);
              }}
              className="btn btn-primary ml-1"
            >
              +
            </button>
          </td>
          <td>
            <img style={{ width: 80 }} src={item.image} alt="" />
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Price</td>
            <td>Quanity</td>
            <td>Image</td>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeReducer.cart,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handlePlusShoe: (shoe) => {
      let action = {
        type: PLUS_SHOE,
        payload: shoe,
      };
      dispatch(action);
    },
    handleMinusShoe: (shoe) => {
      let action = {
        type: MINUS_SHOE,
        payload: shoe,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
