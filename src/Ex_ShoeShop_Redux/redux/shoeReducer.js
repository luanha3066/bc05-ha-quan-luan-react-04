import { dataShoe } from "../dataShoe";
import {
  ADD_TO_CART,
  CHANGE_DETAIL,
  MINUS_SHOE,
  PLUS_SHOE,
} from "./ShoeConstant";
let initialState = { shoeArr: dataShoe, detail: dataShoe[0], cart: [] };
export const shoeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      let copyCart = [...state.cart];
      let index = copyCart.findIndex((shoe) => {
        return shoe.id == action.payload.id;
      });
      // Neu chua co giay trong gio hang
      if (index == -1) {
        let CardItem = { ...action.payload, number: 1 };
        copyCart.push(CardItem);
      } else {
        // Neu da co giay nay trong gio hang
        copyCart[index].number++;
      }
      return { ...state, cart: copyCart };
    }
    case CHANGE_DETAIL: {
      state.detail = action.payload;
      return { ...state };
    }
    case PLUS_SHOE: {
      let cloneCard = [...state.cart];
      let index = cloneCard.findIndex((shoe) => {
        return shoe.id === action.payload.id;
      });
      cloneCard[index].number++;
      return { ...state, cart: cloneCard };
    }
    case MINUS_SHOE: {
      let cloneCard = [...state.cart];
      let index = cloneCard.findIndex((shoe) => {
        return shoe.id === action.payload.id;
      });
      cloneCard[index].number--;
      if (cloneCard[index].number <= 0) {
        cloneCard.splice(index, 1);
      }
      return { ...state, cart: cloneCard };
    }
    default:
      return state;
  }
};
