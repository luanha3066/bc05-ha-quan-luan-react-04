export const ADD_TO_CART = "ADD_TO_CART";
export const CHANGE_DETAIL = "CHANGE_DETAIL";
export const PLUS_SHOE = "PLUS_SHOE";
export const MINUS_SHOE = "MINUS_SHOE";
