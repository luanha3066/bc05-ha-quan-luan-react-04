import React, { Component } from "react";
import { connect } from "react-redux";

class Detail extends Component {
  render() {
    return (
      <div className="row mt-5">
        <img className="col-3" src={this.props.detail.image} alt="" />
        <div className="col-9">
          <p>ID: {this.props.detail.id}</p>
          <p>Ten SP: {this.props.detail.name}</p>
          <p>Gia: {this.props.detail.price}</p>
          <p>Mo ta: {this.props.detail.description}</p>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    detail: state.shoeReducer.detail,
  };
};
export default connect(mapStateToProps)(Detail);
